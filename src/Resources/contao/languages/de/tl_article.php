<?php
/*
 * Copyright Information
 * @copyright: (c) 2023 agentur fipps e.K.
 * @author   : Arne Borchert <arne.borchert@fipps.de>
 * @license  : LGPL 3.0+
 */


// Fields
$GLOBALS['TL_LANG']['tl_article']['hideFromArticleList'] = ['In der Artikelliste verbergen', 'Diesen Artikel nicht in der Artikelliste anzeigen'];