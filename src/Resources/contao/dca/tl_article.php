<?php
/*
 * Copyright Information
 * @copyright: (c) 2023 agentur fipps e.K.
 * @author   : Arne Borchert <arne.borchert@fipps.de>
 * @license  : LGPL 3.0+
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$table = 'tl_article';
$dca = &$GLOBALS['TL_DCA'][$table];

PaletteManipulator::create()
                  ->addField('hideFromArticleList', 'title')
                  ->applyToPalette('default', $table);

$dca['fields']['hideFromArticleList'] = [
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class'=>'w50 m12'),
    'sql'                     => "char(1) NOT NULL default ''"
];
